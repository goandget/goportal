-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 15, 2013 at 06:31 PM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `goPortal`
--

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_bookingaccess`
--

CREATE TABLE IF NOT EXISTS `eventbooking_bookingaccess` (
  `sid` int(6) NOT NULL,
  `id` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `max` int(6) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `booked` int(6) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`sid`,`id`,`type`,`start`,`end`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventbooking_bookingaccess`
--

INSERT INTO `eventbooking_bookingaccess` (`sid`, `id`, `type`, `max`, `start`, `end`, `booked`, `active`) VALUES
(2, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-22 12:28:54', 0, 1),
(1, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-22 12:28:54', 0, 1),
(3, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-22 12:28:54', 0, 1),
(5, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-22 12:28:54', 0, 1),
(6, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-22 12:28:54', 0, 1),
(4, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-22 12:28:54', 0, 1),
(1, 'blue', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(2, 'blue', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(3, 'blue', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(4, 'blue', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(5, 'blue', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(6, 'blue', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(1, 'yellow', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(2, 'yellow', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(3, 'yellow', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(4, 'yellow', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(5, 'yellow', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(6, 'yellow', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(1, 'orange', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(2, 'orange', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(3, 'orange', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(4, 'orange', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(5, 'orange', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(6, 'orange', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(1, 'purple', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(2, 'purple', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(3, 'purple', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(4, 'purple', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(5, 'purple', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(6, 'purple', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(1, 'green', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(2, 'green', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(3, 'green', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(4, 'green', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(5, 'green', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1),
(6, 'green', 'booking', 2, '2013-03-07 12:28:49', '2013-04-11 00:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_bookings`
--

CREATE TABLE IF NOT EXISTS `eventbooking_bookings` (
  `sid` int(6) NOT NULL,
  `uid` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventbooking_bookings`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_events`
--

CREATE TABLE IF NOT EXISTS `eventbooking_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `eventstart` datetime NOT NULL,
  `eventend` datetime NOT NULL,
  `image` varchar(145) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eventbooking_events`
--

INSERT INTO `eventbooking_events` (`id`, `title`, `description`, `eventstart`, `eventend`, `image`, `start`, `end`, `active`) VALUES
(1, 'Flexible Day', 'The Staff Selection for Flexible Day.', '2013-07-10 12:25:48', '2013-07-10 12:25:48', 'flexidays', '2013-03-07 12:26:41', '2013-03-22 12:26:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_group`
--

CREATE TABLE IF NOT EXISTS `eventbooking_group` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `eventbooking_group`
--

INSERT INTO `eventbooking_group` (`id`, `name`) VALUES
(1, 'red'),
(2, 'blue'),
(3, 'yellow'),
(4, 'orange'),
(5, 'purple'),
(6, 'green');

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_groupuser`
--

CREATE TABLE IF NOT EXISTS `eventbooking_groupuser` (
  `gid` int(6) NOT NULL,
  `uid` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`gid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventbooking_groupuser`
--

INSERT INTO `eventbooking_groupuser` (`gid`, `uid`) VALUES
(1, '1'),
(1, '1112'),
(1, '1130'),
(1, '1135'),
(1, '1149'),
(1, '1178'),
(1, '1185'),
(1, '1193'),
(1, '1205'),
(1, '4618'),
(1, '4624'),
(1, '4625'),
(1, '4626'),
(2, '1139'),
(2, '1179'),
(2, '1209'),
(2, '1220'),
(2, '1243'),
(2, '1254'),
(2, '1275'),
(2, '1277'),
(2, '4623'),
(2, '4627'),
(3, '1116'),
(3, '1128'),
(3, '1132'),
(3, '1144'),
(3, '1197'),
(3, '1211'),
(3, '1222'),
(3, '1256'),
(3, '4145'),
(3, '4628'),
(3, '4629'),
(4, '1117'),
(4, '1186'),
(4, '1216'),
(4, '1280'),
(4, '4630'),
(4, '4631'),
(4, '4632'),
(4, '4633');

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_sectors`
--

CREATE TABLE IF NOT EXISTS `eventbooking_sectors` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `eid` int(6) NOT NULL,
  `title` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(245) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extrainformation` text COLLATE utf8_unicode_ci,
  `image` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `max` int(6) NOT NULL DEFAULT '1',
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `order` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `eventbooking_sectors`
--

INSERT INTO `eventbooking_sectors` (`id`, `eid`, `title`, `description`, `extrainformation`, `image`, `max`, `start`, `end`, `order`) VALUES
(1, 1, 'Health', '''Health is Wealth''', NULL, '1-healthservice.png', 12, '2013-03-07 12:27:45', '2013-03-14 12:27:49', 1),
(2, 1, 'Construction Design', '''The need for homes''', NULL, '1-ConstructionDesign.png', 12, '2013-03-13 11:17:12', '2013-03-22 11:17:19', 2),
(3, 1, 'Media', '''Immortalise your Flexi-Day<br /> in exciting and innovative ways!''', NULL, '1-media.png', 12, '2013-03-13 11:18:26', '2013-03-22 11:18:30', 3),
(4, 1, 'Future Concepts', '''In a world of infinite possibilities''', NULL, '1-futureconcepts.png', 12, '2013-03-13 11:19:25', '2013-03-22 11:19:30', 4),
(5, 1, 'Entrepreneurship', '''When greed is good''<br />How can you turn a penny in to a pound?', NULL, '1-Entrepreneurship.png', 12, '2013-03-13 11:19:25', '2013-03-22 11:19:25', 5),
(6, 1, 'Leisure', '''Welcome to Wigan making our mark''', NULL, '1-leisure.png', 12, '2013-03-13 11:21:13', '2013-03-22 11:21:17', 6);

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_tracking`
--

CREATE TABLE IF NOT EXISTS `eventbooking_tracking` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` int(6) NOT NULL,
  `uid` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eventbooking_tracking`
--

