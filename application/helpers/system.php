<?php

/*	##############################################################
 *	Helper Functions, School Specific for the whole system
 *	
 *
 *	##############################################################
 */

/**
 *	Return the Current Accademic Year Start Date (Assuming Start on the 1st August)
 *  @access Public
 *  @param 	array
 *  @return array
 */
public function _current_accademic_year()
{
	if (date('m') >= 8)
	{
		return date('Y').'-08-01';
	}
	else
	{
		return date('Y')-1.'-08-01';
	}
}