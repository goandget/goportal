<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Go Auth
 *
 * Author: Jonathan Hooton
 *		   jonathan@goandget.co.uk
 *         @jonathanhooton
 *
 * Built from the basis of:  Ion Auth
 * Author: Ben Edmunds, ben.edmunds@gmail.com, @benedmunds
 * Added Awesomeness: Phil Sturgeon
 *
 * Location: http://github.com/goandget/goAuth
 *
 * Created:  2013-02-26
 *
 * Description:  Modified auth system based on ion_auth with customization to be used within a school network system.
 * Although the original author is sited there isn't necessary any method that hasn't been altered.
 *
 * Requirements: PHP5 or above
 *
*/

class go_auth
{

	/**
	 * account status
	 *
	 * @var string
	 *
	 **/
	protected $status;

	/**
	 * __construct
	 *
	 * @return void
	 **/
	public function __construct()
	{
		$this->load->config('go_auth', TRUE);
		$this->load->library('email');
		$this->load->helper('cookie');

		$this->load->library('session');
		$this->load->model('go_auth');

		// Auto-Login the user if they are either remembered or have a NTLM authentication.
		if (!$this->logged_in() && get_cookie('identity') && get_cookie('remember_code'))
		{
			// Log in the remembered user as per cookie
			$this->go_auth_model->login_remembered_user(get_cookie('remember_code'),get_cookie('identity'));
		}
		else if (!$this->logged_in() && get_ntlm('identity'))
		{
			// Log in the user if identified via ntlm
			$this->go_auth_model->login_remembered_user('ntlm',get_ntlm('identity'));
		}

	}

	/**
	 * __call
	 *
	 * Acts as a simple way to call model methods.
	 *
	**/
	public function __call($method, $arguments)
	{
		if (!method_exsists( $this->go_auth, $method))
		{
			throw new Exception('Undefined method go_auth:'.$method.'() called');
		}

		return call_user_func_array( array($this->go_auth, $method),$arguments);
	}

	



}