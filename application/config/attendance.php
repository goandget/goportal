<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Attendance Data Configuration
| -------------------------------------------------------------------------
| All the Configuration needs for attendance related variables
|
*/


/*
| -------------------------------------------------------------------------
| Attendance Marks
| -------------------------------------------------------------------------
*/
$config['present'] = array('/','\\','L','B','P','S','V','W','Y');
$config['nomark'] = array('#','-');
$config['late'] = array('L','U');


/*
| -------------------------------------------------------------------------
| Accademic Year Renewal
| -------------------------------------------------------------------------
*/
$config['current_accademic_year'] = '2012-08-28';