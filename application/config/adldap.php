<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['account_suffix']		= '@arrowsmith';
$config['base_dn']				= 'DC=arrowsmith,DC=wigan,DC=sch,DC=uk';
$config['domain_controllers']	= array ("arrowsmith.wigan.sch.uk");
$config['ad_username']			= 'administrator';
$config['ad_password']			= 'akinyemi';
$config['real_primarygroup']	= true;
$config['use_ssl']				= false;
$config['use_tls'] 				= false;
$config['recursive_groups']		= true;


/* End of file adldap.php */
/* Location: ./system/application/config/adldap.php */