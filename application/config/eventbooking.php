<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Event Booking Data Configuration
| -------------------------------------------------------------------------
| All the Configuration needs for attendance related variables
|
*/


/*
| -------------------------------------------------------------------------
| CSS Colour Stylesheets
| -------------------------------------------------------------------------
*/
$config['colors'] = array('green','blue','terquiose','orange','yellow','purple','pink','lightblue');
