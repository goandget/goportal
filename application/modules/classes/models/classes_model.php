<?php

class classes_model extends Model {
	
	get_classes($uid=false,$type=false)
	{
		$this->db->select('Class');
		switch($type)
		{
			case 'pupil':
				$this->db->where('upn', $uid);
				break;
			case 'staff':
				$this->db->where('Staff ID', $uid);
				break;
		}
		$this->db->group_by('Class');
		$query = $this->db->get('sims_classes');
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else {
			return false;
		}
	}
	
}
