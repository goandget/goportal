<?php

class senregister extends Controller {

	function __construct()
	{
		parent::Controller();

		// Check User is logged in.
		modules::run('login/is_logged_in');

		// Load any config files necessary
		$this->config->load('attendance');
	}
	
	
	function index()
	{

		// Load the Correct Model to get class lists
		$this->load->model('classes_model');	

		// Get Current Users Details
		$details = modules::run('login/get_user_details');

		// Get the List of Classes
		$classes = $this->classes_model->get_classes($details['simsid'],$this->session->userdata('usertype'));

		// Set Current Class Variable
		$listofclasses = array();

		// Set Current Class Variable
		$curclass = array();
		
		// Find the class that is either on now or due next
		foreach ($classes as $cls)	
		{
			$listofclasses[$cls->Class] = TRUE;
			if (!isset($curclass['class']))	
			{
				$curclass['class'] = $cls->Class;
				$curclass['starttime'] = $cls->Starttime;
				$curclass['endtime'] = $cls->Endtime;

			}
			else if ((strtolower($cls->day) == strtolower(date("D")))||($cls->day == ''))	
			{
				
					if ((date("H:i:s") < $cls->Endtime)&&(date("H:i:s") > $cls->Starttime))
					{
						// If the class is the current class then. 
						$curclass['class'] = $cls->Class;
						$curclass['starttime'] = $cls->Starttime;
						$curclass['endtime'] = $cls->Endtime;
					}
					else if (($cls->day == '')&&(date("H:i:s") > $curclass['endtime'])&&(date("H:i:s") < $curclass['starttime']))
					{
						// If there is no better option then put the form as default list
						$curclass['class'] = $cls->Class;
						$curclass['starttime'] = $cls->Starttime;
						$curclass['endtime'] = $cls->Endtime;
					}
			}
		}

		$classdetail = $this->get_classdetail($curclass['class'],'pupildata','pupildata_error');

		$data = array('classes' => $listofclasses,'curclass' => $curclass['class'],'classdetail' => $classdetail);
		$this->template->set_content('senregister', $data);
		$this->template->add_js('senregister/js/senregister');
		$this->template->add_css('senregister/css/senregister');
		$this->template->build();

	}
	

	function test()	
	{
		$data = array(1,2,3,4);
		return $data;
		
	}
	
	private function get_classes()
	{
	}

	public function ajax_classdetail()
	{
		echo $this->get_classdetail($this->input->post('curclass'),'pupildata','pupildata_error');
	}

	private function get_classdetail($curclass=false,$view=false,$view_error=false)
	{

		// Get the current classes data

		// Load the Correct Model to get class lists
		$this->load->model('class_model');	

		// Get the class list
		$classlist = $this->class_model->get_classlist($curclass);

		// Set up the class list from view.
		$classdetail = '';

		if (is_array($classlist))
		{
			// Make sure the data is empty
			$data = array('upn' => '','need' => array());

			$first = true;
			foreach ($classlist as $pupil)
			{

				if ($data['upn'] != $pupil->upn)	{

					if (!$first)	{

						//echo $pupil->first_name.' '.$pupil->last_name.' '.$pupil->upn;
						$classdetail.= $this->load->view($view, $data, true);

						// Empty the data is empty
						$data = array('upn' => '','need' => array());
					}

					// Get the pupils 
					$formdetails = $this->class_model->get_formdetails($pupil->upn);

					$data['upn'] = $pupil->upn;
					$data['forename'] = $pupil->first_name;
					$data['surname'] = $pupil->last_name;
					$data['gender'] = $pupil->gender;
	 				$data['form'] = $formdetails['form'];
					$data['teacher'] = $formdetails['teacher'];
					$data['attendance'] = $this->get_percentageattendance($pupil->upn);

				}

				array_push($data['need'],array('title' => $pupil->need));
				$first = false;

			}

			// Send the last data set to the view
			$classdetail.= $this->load->view($view, $data, true);
			//$classdetail[] = $this->load->view('pupildata', $data, true);
		}
		else
		{
			$classdetail = $this->load->view($view_error, '', true);
		}	
		return $classdetail;
	}

	public function get_percentageattendance($uid=false,$start=false,$end=false)
	{
		
		if ($uid)
		{

			// Load the Correct Model to get attendance lists
			$this->load->model('attendance_model');	

			// Set the Marks Array
			$marks = array();

			if (!$start)	{
				// Default Start Date 1st August this accedemic year
				if (date("n") > 8)	{
					$start = date("Y").'-08-01';
				}
				else 
				{
					$start = (date("Y")-1).'-08-01';
				}
			}
			else {
				$start = date("Y-m-d",strtotime($start));
			}

			if (!$end)	{
				// Default Start Date 1st August this accedemic year
				if (date("n") > 8)	{
					$end = (date("Y")+1).'-07-31';
				}
				else 
				{
					$end = date("Y").'-07-31';
				}
			}
			else {
				$end = date("Y-m-d",strtotime($end));
			}
			
			// Set the Marks Array
			$marks = $this->attendance_model->get_attendance($start,$end,$uid);
			
			$present = $this->config->item('present');
			$dontcount = $this->config->item('nomark');

			$in = 0;
			$total = 0;
			foreach ($marks as $v)	{
				if (in_array($v->Mark,$present))	{
					$in = $in + $v->Marks;
					$total = $total + $v->Marks;
				}
				else if (!in_array($v->Mark,$dontcount))
				{
					$total = $total + $v->Marks;
				}
			}

			return round(($in*100)/$total,1).'%';

		}

	}
	
	// Ajax Public Function To return the need data
	public function ajax_needdetails()
	{

		if ($need=$this->input->post('needs'))
		{
			// Load the Correct Model to get need details
			$this->load->model('needs_model');	

			// Get the stratergies for the need
			$needdetail = $this->needs_model->get_needdetail($need);

			// Get the stratergies for the need
			$strategies = $this->needs_model->get_strategies($need);
			
			$data['need'] = $needdetail['need'];
			$data['description'] = $needdetail['description'];
			$data['strategies'] = $strategies;

			echo $this->load->view('needdata', $data, true);
			//echo 'done';
		}

	}
}