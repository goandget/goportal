<?php

class classes_model extends Model {
	
	function get_classes($uid=false,$type=false)
	{

		$this->db->select('Class,day,Starttime,Endtime');
		$this->db->from('sims_classes');
		$this->db->join('system_timetable', 'system_timetable.period = sims_classes.Period');
		switch($type)
		{
			case 'pupil':
				$this->db->where('upn', $uid);
				break;
			case 'staff':
				$this->db->where('`Staff ID` =', $uid,FALSE);
				break;
		}
		$this->db->group_by(array('Class','day','Starttime','Endtime'));
		$this->db->order_by('CAST(Class as SIGNED)', 'ASC');
		$this->db->order_by('Class', 'ASC');
		$query = $this->db->get();
		
		/*if ($config['debug'])	{
			echo '<hr>Classes Model: Get_CLasses<br />';
			echo $this->db->last_query();
			echo '<hr>';
		}*/

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else {
			return false;
		}
	}
	
}
