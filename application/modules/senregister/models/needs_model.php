<?php

class needs_model extends Model {

	function get_needdetail($need=false)
	{

		if ($need)	
		{
			// Select the class information needed
			$this->db->select('need,senregister_needs.Description as description');
			$this->db->where('need',$need);
			$query = $this->db->get('senregister_needs');
			//echo $this->db->last_query();
			if($query->num_rows == 1)
			{
				// Get the last row if there are more than one
				$row = $query->last_row();
				// Assign the row to our return array
				$data['need'] = $row->need;
				$data['description'] = $row->description;
				return $data;
			}
			else {
				return false;
			}
		}
	}

	function get_strategies($need=false)
	{
		// Return the full list of strategies.
		$this->db->select('strategy');
		$this->db->where('need',$need);
		$query = $this->db->get('senregister_strategies');

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
}