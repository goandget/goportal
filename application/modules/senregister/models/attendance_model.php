<?php

class attendance_model extends Model {

	function get_attendance($start=false,$end=false,$uid=false)
	{

		$this->db->select('Count(UPN) as Marks,Mark');
		if ($start)
		{
			$this->db->where('`Mark date` > "', $start .'"',FALSE);
		}
		if ($end)
		{
			$this->db->where('`Mark date` < "', $end .'"',FALSE);
		}
		if ($uid)
		{
			$this->db->where('upn', $uid);
		}
		
		$this->db->group_by(array('Mark'));

		$query = $this->db->get('sims_attendance');
		
		//echo $this->db->last_query();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else {
			return false;
		}
		
	}

}