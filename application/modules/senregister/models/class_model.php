<?php

class class_model extends Model {

	function get_classlist($class=false)
	{

		if ($class)	
		{
			// Select the class information needed
			$this->db->select('`system_users`.`simsid` as upn,`first_name`,`last_name`,`gender`,`need type code` as need',FALSE);
			$this->db->from('system_users');
			$this->db->join('sims_classes','sims_classes.upn=system_users.simsid');
			$this->db->join('sims_senneeds','sims_senneeds.UPN=system_users.simsid');
			$this->db->where('class',$class);
			$this->db->where('`need type code` !=','\'\'',FALSE);
			$this->db->group_by(array('simsid'));
			$query = $this->db->get();
			//echo $this->db->last_query();
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else {
				return 'none';
			}
		}

	}

	function get_formdetails($upn=false)	
	{
		if ($upn)
		{
			// Set the fields to be selected
			$this->db->select('Teacher,Class');
			// Set the filtering
			$this->db->where('subject','');
			$this->db->where('upn',$upn);
			// Get the data from the table
			$query = $this->db->get('sims_classes');

			if($query->num_rows() > 0)
			{
				// Get the last row if there are more than one
				$row = $query->last_row();
				// Assign the row to our return array
				$data['teacher'] = $row->Teacher;
				$data['form'] = str_replace('CLS ','',$row->Class);
				// Return the form details found
				return $data;
			}
		}
	}
}