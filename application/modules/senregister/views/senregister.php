<!-- Main Content Area -->
	<div id="displaybox">
            <div class="diaplytabs">
                <ul>
                    <li class="active">SEN</li>
                </ul>
            </div>
            <div id="displaycontent">
                <div id="container2">
                    <div id="container1">
                        <div id="col1">
                            <ul>
                            	<?php foreach ($classes as $cls => $v):?>
                                <li class="menucls<?php if ($cls == $curclass) {?> active<?php } ?>"><span><?php echo $cls;?></span></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        <div id="col2">
                            <div class="displaymain">
                                <h1>Special Educational Needs</h1>
                                    <div class="pupildetail-container"><?php echo $classdetail; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clr-b"></div>
            </div>
    </div>