<?php

class Login extends Controller {
	
	function index()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
			if(!isset($is_logged_in) || $is_logged_in != true)
			{
				$data =array();
				$this->template->set_content('login_form', $data);
				$this->template->add_css('login/css/loginform');
				$this->template->add_js('login/js/login');
				$this->template->build();	
			}
			else {
				// If user logged in then transfer to dashboard
				redirect('pages/dashboard');
			}
	}
	
	function validate_credentials()
	{		
		$this->load->model('system_users_model');
		$data = array(
			'username' => $this->input->post('username')
		);
		$usertype = $this->system_users_model->get_usertype($data);
		//print_r($usertype);
		$query = false;
		switch ($usertype->authtype)	
		{
			case 'mysql':
				$query = $this->system_users_model->validate();
				break;
			case 'ad':
				$this->load->library('Adldap');
				if ($usertype->type == 'pupil')	{
					//$query = $this->adldap->authenticate($this->input->post('username'),$this->input->post('password'));
					$query = FALSE;
				}
				else
				{
					$this->adldap->set_domain_controllers(array ("admin.arrowsmith.wigan.sch.uk"));
					$this->adldap->set_account_suffix("@admin.arrowsmith.wigan.sch.uk");
					$this->adldap->set_base_dn("ou=staff,DC=admin,DC=arrowsmith,DC=wigan,DC=sch,DC=uk");
					$query = $this->adldap->authenticate($this->input->post('username'),$this->input->post('password'));
				}
				break;
			default:
				$this->template->add_message('error_title','Sorry, User');
				$this->template->add_message('error_message','Really sorry but you ware not a registered user on our system.');
				$data =array();
				$this->template->set_content('login_form', $data);
				$this->template->build();
		}
		
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('username'),
				'usertype' => $usertype->type,
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('pages/dashboard');
		}
		else // incorrect username or password
		{
			$this->index();
		}
	}
	
	function offline()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');
			if(!isset($is_logged_in) || $is_logged_in != true)
			{
				$data =array();
				$this->template->set_content('offline', $data);
				$this->template->add_css('login/css/loginform');
				$this->template->add_js('login/js/login');
				$this->template->build();	
			}
			else {
				// If user logged in then transfer to dashboard
				redirect('pages/dashboard');
			}
	}
	
	function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('is_logged_in');
		$this->session->sess_destroy();
		$this->index();
	}	
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			echo 'You don\'t have permission to access this page. <a href="'.base_url().'">Login</a>';	
			die();		
			//$this->load->view('login_form');
		}		
	}
	
	function get_user_details()
	{
		// load the model for this controller
		$this->load->model('system_users_model');
		// Get User Details from Database
		$user = $this->system_users_model->get_member_details();
		if( !$user )
		{
			// No user found
			return false;
		}
		else
		{
			// Manually Add in the Roles at this moment in time
			$user['roles'] = array('staff');
			// display our widget
			return $user;
		}
	}

	function cp()
	{
		if( $this->session->userdata('username') )
		{
			// load the model for this controller
			$this->load->model('system_users_model');
			// Get User Details from Database
			$user = $this->system_users_model->get_member_details();
			if( !$user )
			{
				// No user found
				return false;
			}
			else
			{
				// display our widget
				$this->load->view('user_widget', $user);
			}			
		}
		else
		{
			// There is no session so we return nothing
			return false;
		}
	}
}