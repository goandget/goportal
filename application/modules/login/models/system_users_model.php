<?php

class system_users_model extends Model {

	function validate()
	{
		$this->db->where('username', $this->input->post('username'));
		$this->db->where('password', md5($this->input->post('password')));
		$query = $this->db->get('system_users');
		//echo $this->db->last_query();
		if($query->num_rows == 1)
		{
			return true;
		}
	}
	
	function create_member($data=array())
	{
		
		$new_member_insert_data = array(
			'forename' => $data['first_name'],
			'surname' => $data['last_name'],
			'email_address' => $data['email_address'],			
			'username' => $data['username'],
			'password' => md5($data['password']),
			'saved_password' => $data['password'],
			'user_type' => $data['user_type']
		);
		
		$insert = $this->db->insert('system_users', $new_member_insert_data);
		return $insert;
	}
	
	function get_member_details($id=false)
	{
		if( !$id )
		{
			// Set Active Record where to the current session's username
			if( $this->session->userdata('username') )
			{
				$this->db->where('username', $this->session->userdata('username'));
			}
			else
			{
				// Return a non logged in person from accessing member profile dashboard
				return false;
			}
		}
		else
		{
			// get the user by id
			$this->db->where('id', $id);
		}
		// Find all records that match this query
		$query = $this->db->get('system_users');
		// In this case because we don't have a check set for unique username 
		// we will return the last user created with selected username.
		if($query->num_rows() > 0)
		{
			// Get the last row if there are more than one
			$row = $query->last_row();
			// Assign the row to our return array
			$data['id'] = $row->id;
			$data['first_name'] = $row->first_name;
			$data['last_name'] = $row->last_name;
			$data['simsid'] = $row->simsid;
			// Return the user found
			return $data;
		}
		else 
		{
			// No results found
			return false;
		}
	}
	
	function get_usertype($data = false)
	{
		$this->db->select('authtype,system_usertypes.type as type');
		$this->db->from('system_users');
		$this->db->join('system_usertypes', 'system_users.type = system_usertypes.utid');
		$this->db->where('username',$data['username']);
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows == 1)
		{
			return $query->row();
		}
	}
}