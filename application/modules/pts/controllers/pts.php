<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Controller For the PTS App
| -------------------------------------------------------------------
| This file will contain all the functions for the app and send the data
| to the view files
*/

class pts extends Controller {

	function __construct()
	{
		parent::Controller();
	}

	/**
	 *	Index Function
	 *  By Default what is shown by default.
	 *  @access Public
	 *  @param 	string
	 *  @return string
	 */
	function index()
	{
		// Load PTS model
		$this->load->model('pts_model');

		// Get Current Users Details
		$details = modules::run('login/get_user_details');	

		if (!in_array('staff',$datails['roles'])) {
			modules::run('login/no_permission');
		}

		// If user has a head/deputy head of year then show their year results page
		if (((in_array('headofyear',$datails['roles']))||(in_array('deputyheadofyear',$datails['roles'])))&&(!$this->input->get_post('s',TRUE)))
		{
			$filter = $this->pts_model->get_year($details['user_id']);

			$data = $this->_search('totals',$filter);

			$this->_view('totals',array('filter' => $filter,'data' => $data));
		}
		// If the user is a form teacher then show their form entry
		elseif ($class = $this->pts_model->get_teachers_form($details['simsid']))
		{
			$filter = array('class' => $class);

			$data = $this->_search('entry',$filter);

			$this->_view('entry',array('filter' => $filter,'data' => $data));

		}
		// If the user has a favourite search or has one set as default then show the resulting search
		elseif ($filters = $this->pts_model->get_searches(array('user_id' => $details['user_id'])))
		{
			$array = explode(',',$filters);

			foreach ($array as $v)
			{
				$i = explode('=>',$v);
				$filter[$i[0]] = $i[1];
			}

			$type = $filter['type'];
			unset($filter['type']);

			$data = $this->_search($type,$filter);


			$this->_view($type,array('filter' => $filter,'data' => $data));

		}
		// Else just show the search area
		else
		{
			$this->_view('search');
		}
	}

	/**
	 *	Index Function
	 *  By Default what is shown by default.
	 *  @access Public
	 *  @param 	string
	 *  @return string
	 */
	function save_points()
	{
		
	}

	/**
	 *	Search Function
	 *  By Default what is shown by default.
	 *  @access Public
	 *  @param 	string
	 *  @return string
	 */
	private function _search($type = 'totals',$data = array())
	{	
		// If data entry get the data for the week.
		if ($type = 'entry')
		{
			if (isset($data['weekcom']))
			{
				// Searching for a Specific weeks PTS Points
				$clsdata = $this->pts_model->get_pts_data($data);
			}
			else
			{
				// Current Weeks PTS Points
				$clsdata = $this->pts_model->get_pts_data($data,array('weekcom >' => date("Y-m-d",strtotime('-7 day'.now()))));
			}
		}
		// Now get the totals for the 
		if ($this->_array_ikey_exists('weekcom',$data))
		{
			$clstotals = $this->_get_pts_totals($data);
			$clsdata = $this->_get_attendance(array('filter' => $data,'data' => $clsdata));
		}
		else
		{
			$clstotals = $this->_get_pts_totals($data,array('weekcom >' => $this->config->item('current_accademic_year','attendance')));
			$clsdata = $this->_get_attendance(array('filter' => array('weekcom >' => $this->config->item('current_accademic_year','attendance')),'data' => $clsdata));
		}

		return array('data'=>$clsdata,'totals'=>$clstotals);
	}

	/**
	 *	Display the results Function
	 *  By Default what is shown by default.
	 *  @access Public
	 *  @param 	string
	 *			type of view
	 *  @param 	array
	 *			data to pass to view
	 */
	private function _view($type = 'totals',$data = array())
	{
		//$data = array('classes' => $listofclasses,'curclass' => $curclass['class'],'classdetail' => $classdetail);
		$this->template->set_content('pts_'.$type, $data);
		$this->template->add_js('pts/js/pts');
		$this->template->add_css('pts/css/pts');
		$this->template->build();
	}

	/**
	 *	Get PTS Totals Function
	 *  @access Private
	 *  @param 	array
	 *  @return array
	 */
	private function _get_pts_totals($filter = array())
	{
		$data = $this->pts_model->get_pts_totals($filter);

		foreach ($data as $row)
		{
			$clstotal[$row->user_id] = $row;
		}

		return $clstotal;
	}

	/**
	 *	Get Attendance Details Function
	 *  @access Private
	 *  @param 	array
	 *  @return array - parameter array + the attendance data
	 */
	private function _get_attendance($array = array())
	{
		foreach ($array['data'] as $row)
		{
			$clsdata[$row->user_id] = $row;
		}

		$data = $this->pts_model->get_attendance($array['filter']);

		foreach ($data as $row)
		{
			if (!isset($clsdata[$row->user_id]['total']))
			{
				$clsdata[$row->user_id]['present'] = 0;
				$clsdata[$row->user_id]['total'] = 0;
				$clsdata[$row->user_id]['late'] = 0;	
			}

			if (in_array($row->Mark,$this->config->item('present','attendance')))	
			{
				$clsdata[$row->user_id]['present'] = $clsdata[$row->user_id]['present'] + $row->Marks;
				$clsdata[$row->user_id]['total'] = $clsdata[$row->user_id]['total'] + $row->Marks;
			}
			else if (!in_array($row->Mark,$this->config->item('nomark','attendance')))
			{
				$clsdata[$row->user_id]['total'] = $clsdata[$row->user_id]['total'] + $v->Marks;
			}
			if (in_array($row->Mark,$this->config->item('late','attendance')))	
			{
				$clsdata[$row->user_id]['late'] = $clsdata[$row->user_id]['late'] + $v->Marks;
			}

		}

		return $clsdata;

	}

	/**
	 *	Get Attendance Details Function
	 *  @access Private
	 *  @param 	string 	- the key
	 *	@param 	array 	- the array to search
	 *  @return boolean
	 */
	private function _array_ikey_exists($key,$arr) 
	{ 
	    if(preg_match("/\b".$key."\b/", join(",", array_keys($arr)))) 
	    {
	        return true; 
	    }               
	    else 
	    {
	        return false; 
	    }
	}