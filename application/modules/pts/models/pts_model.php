<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Model For the PTS App
| -------------------------------------------------------------------
| This file will contain all the functions to connect with the PTS
| Database.
*/

class pts_model extends Model {

/**
 *	Get Teachers Form
 *  @access Public
 *  @param 	string
 *  @return string
 */
public function get_teachers_form($userid)
{
	$this->db->select('class');
	$this->db->where('Staff Code',$userid);
	$this->db->like('Class','CLS ','after');
	$query = $this->db->get('sims_classes');

	if($query->num_rows() > 0)
	{
		$row = $query->row_array();
		return $row->class;
	}

	else
	{
		return false;
	}

}

/**
 *	Get PTS Data
 *  @access Public
 *  @param 	array
 *  @return array
 */
public function get_pts_data($array = array())
{
	$this->db->select('user_id,first_name,last_name,uniform,detentions,homework,planners,housepoints');
	$this->db->join('system_pupils','system_pupils.user_id = system_users.id');
	$this->db->join('pts_points','pts_points.user_id = system_users.id','left');

	// Set the Filtering of the Query
	if (isset($array['filter']))
	{
		foreach ($array['filter'] as $k => $v)
		{
			if ($k == 'class')
			{
				$this->db->join('sims_classes','sims_classes.UPN = system_users.UPN');
			}
			$this->db->where($k, $v);
		}
	}
	// Set the Filtering Default if none sent
	else
	{
		$this->db->where('weekcom >',date("Y-m-d",strtotime('-7 day'.now())));
	}

	$this->db->group_by(array('system_users.UPN'));

	// Set the Sort Order of the Query
	if (isset($array['sort']))
	{
		foreach ($array['sort'] as $k => $v)
		{
			$this->db->order_by($k, $v);
		}
	}
	// Set the Sort Order if none specified
	else
	{
		$this->db->order_by('last_name', 'ASC');
		$this->db->order_by('first_name', 'ASC');
	}

	$query = $this->db->get('system_users');


	if($query->num_rows() > 0)
	{
		return $query->result();
	}

	else
	{
		return false;
	}

}

/**
 *	Get PTS Totals
 *  @access Public
 *  @param 	array
 *  @return array
 */
public function get_pts_totals($array = array())
{
	$this->db->select('user_id,first_name,last_name,IFNULL(SUM(uniform),0),IFNULL(SUM(detentions),0),IFNULL(SUM(homework),0),IFNULL(SUM(planners),0),IFNULL(SUM(housepoints),0)');
	$this->db->join('system_pupils','system_pupils.user_id = system_users.id');
	$this->db->join('pts_points','pts_points.user_id = system_users.id','left');

	// Set the Filtering of the Query
	if (isset($array['filter']))
	{
		foreach ($array['filter'] as $k => $v)
		{
			if ($k == 'class')
			{
				$this->db->join('sims_classes','sims_classes.UPN = system_users.UPN');
			}
			$this->db->where($k, $v);
		}
	}
	// Set the Filtering Default if none sent
	else
	{
		$this->db->where('weekcom >',$this->config->item('current_accademic_year','pts'));
	}

	$this->db->group_by(array('system_users.UPN'));

	// Set the Sort Order of the Query
	if (isset($array['sort']))
	{
		foreach ($array['sort'] as $k => $v)
		{
			$this->db->order_by($k, $v);
		}
	}
	// Set the Sort Order if none specified
	else
	{
		$this->db->order_by('last_name', 'ASC');
		$this->db->order_by('first_name', 'ASC');
	}

	$query = $this->db->get('system_users');


	if($query->num_rows() > 0)
	{
		return $query->result();
	}

	else
	{
		return false;
	}

}

/**
 *	Get Attendance Percentage
 *  @access Public
 *  @param 	array
 *  @return array
 */
public function get_attendance($array = array())
{

	$this->db->select('upn,Count(UPN) as Marks,Mark');
	$this->db->join('sims_attendance','sims_attendance.upn = system_users.upn');
	$this->db->join('system_pupils','system_pupils.user_id = system_users.id');

	// Set the Filtering of the Query
	if (isset($array['filter']))
	{
		foreach ($array['filter'] as $k => $v)
		{
			if ($k == 'class')
			{
				$this->db->join('sims_classes','sims_classes.UPN = system_users.UPN');
			}
			$this->db->where($k, $v);
		}
	}
	
	$this->db->group_by(array('Mark','system_users.UPN'));

	// Set the Sort Order of the Query
	if (isset($array['sort']))
	{
		foreach ($array['sort'] as $k => $v)
		{
			$this->db->order_by($k, $v);
		}
	}
	// Set the Sort Order if none specified
	else
	{
		$this->db->order_by('last_name', 'ASC');
		$this->db->order_by('first_name', 'ASC');
	}

	$query = $this->db->get('system_users');
	
	//echo $this->db->last_query();

	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	else {
		return false;
	}

}

/**
 *	Get List of Classes
 *  @access Public
 *  @param 	array
 *  @return array
 */
public function get_classes($array = array())
{
	$this->db->select('class,teacher,period');
	$this->db->join('sims_classes','sims_classes.UPN = system_users.UPN');

	// Set the Filtering of the Query
	if (isset($array['filter']))
	{
		foreach ($array['filter'] as $k => $v)
		{
			$this->db->where($k, $v);
		}
	}
	// Set the Sort Order of the Query
	if (isset($array['sort']))
	{
		foreach ($array['sort'] as $k => $v)
		{
			$this->db->order_by($k, $v);
		}
	}
	// Set the Sort Order if none specified
	else
	{
		$this->db->order_by('CAST(class as SIGNED)', 'ASC');
		$this->db->order_by('class', 'ASC');
	}

	if($query->num_rows() > 0)
	{
		return $query->result();
	}
	else {
		return false;
	}
}

/**
 *	Record the Search
 *  @access Public
 *  @param 	array
 *  @return boolean
 */
public function record_search($array = array())
{
	if ($this->db->insert('pts_searches',$array))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
}

/**
 *	Save PTS Points
 *  @access Public
 *  @param 	array
 *  @return boolean
 */
public function save_points($array = array())
{
	if ($this->db->on_duplicate('pts_points',$array))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
}

/**
 *	Get Searches
 *  @access Public
 *  @param 	string
 *  @param 	array
 *  @return array
 */
public function get_searches($all = 1,$array = array())
{
	$this->db->select('searchvariables,COUNT(searchvariables) as times,default');

	// Set the Filtering of the Query
	if (isset($array['filter']))
	{
		foreach ($array['filter'] as $k => $v)
		{
			$this->db->where($k, $v);
		}
	}
	// Set the Sort Order of the Query
	if (isset($array['sort']))
	{
		foreach ($array['sort'] as $k => $v)
		{
			$this->db->order_by($k, $v);
		}
	}
	// If a result has been returned
	if($query->num_rows() > 0)
	{
		// if just one result needs to be returned
		if ($all == 1)
		{

		}
		// Give the full list of results
		else
		{
			return $query->result();
		}
	}
	else {
		return false;
	}
}

}