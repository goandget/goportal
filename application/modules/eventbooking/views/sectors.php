<?php $i = 1; ?>
<?php foreach ($sectors as $s):?>
<div class="sector <?php if (($s->rolebooked/$s->rolemax != 1)&&($userbooked != $s->id)) {?>bookable<?php } ?>" id="<?php echo $s->id;?>">
 	<div class="arrow_box <?php echo $s->color;?>-box float-l">
 		<div class="arrow-content">
 			<div class="opt"><?php echo $i; ?></div>
 			<img class="float-l" src="<?php echo base_url();?>assets/eventbooking/img/<?php echo $s->image;?>" alt="NHS Option" />
 			<div class="clr-b"></div>
 		</div>
 	</div>
 	<div class="info float-l <?php echo $s->color;?>-info">
 		<h3><?php echo $s->title;?></h3>
 		<?php echo $s->description;?>
 	</div>
 		<div class="<?php echo $s->color;?>-meter meter">
			<span style="width: <?php echo round($s->rolebooked/$s->rolemax*100);?>%"></span>
		</div>
        <div class="ribbon-wrapper">
        	<div class="ribbon <?php echo $s->color;?>-ribbon <?php if ($userbooked == $s->id) {?>booked-ribbon<?php } ?>">
        		<?php if ($userbooked == $s->id) {?>BOOKED<?php } else if ($s->rolebooked/$s->rolemax != 1) {?>BOOK NOW<?php } else { ?>FULL<?php } ?>
        	</div>
        </div>
 	<div class="clr-b"></div>
 </div>
 <?php $i++; ?>
 <?php endforeach;?>
