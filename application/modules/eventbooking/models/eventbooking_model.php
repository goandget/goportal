<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Model for the EVENT BOOKING APPLICATION
| -------------------------------------------------------------------
| This file will contain all the functions to connect with the 
| Database relevant to the event booking application.
*/


class eventbooking_model extends Model {

/**
 *	GET EVENTS
 *  The function will return all the details for the events that 
 * 	are availble to the user at this time.
 *  @access Public
 *  @param 	string
 *				start 	=> start date and time
 *  @param 	string
 *				end 	=> end date and time
 *  @param 	string
 *				active  => if it is active or not.
 *  @param 	array	
 *				roles 	=> all the access roles the user has.			
 *  @return array
 *				return array of all the events.
 */
function get_events(&$start,&$end,$active = false,$roles = false)
{
	if (!isset($start))
	{
		$start  = date('Y-m-d H:i:s');
	}
	if (!isset($end))
	{
		$end  = date('Y-m-d H:i:s');
	}
	// Select Only Certain Fields
	$this->db->select('eventbooking_events.id as id,eventbooking_events.title as title,eventbooking_events.description as description,eventstart,eventend,eventbooking_events.image as  image');

	// Join the tables needed.
	$this->db->join('eventbooking_sectors','eventbooking_sectors.eid=eventbooking_events.id');
	$this->db->join('eventbooking_bookingaccess','eventbooking_bookingaccess.sid=eventbooking_sectors.id');


	// Set the where's
	if ($active)
	{
		$this->db->where('eventbooking_events.active',$active);
	}
	if ($roles && is_array($roles))
	{
		$this->db->where_in('eventbooking_bookingaccess.id',$roles);
	}

	$this->db->where('eventbooking_events.start <=',$start);
	$this->db->where('eventbooking_events.end >=',$end);

	// Group By
	$this->db->group_by('id');

	$query = $this->db->get('eventbooking_events');

	if($query->num_rows() > 0)
	{
		return $query->result();
	}

	else
	{
		return false;
	}

}

/**
 *	GET USERS GROUPS
 *  The function will return all the event booking specific groups for the user  
 *
 *  @access Public
 *  @param 	string
 *				user_id 	=> The current users id		
 *  @return array
 *				return array of all the groups they are assigned to.
 */
function get_users_groups($user_id = false)
{
	if ($user_id)
	{
		// Set the Select
		$this->db->select('name');

		// Set the joins
		$this->db->join('eventbooking_groupuser','eventbooking_groupuser.gid=eventbooking_group.id');

		// Group By 
		$this->db->group_by('name');

		// Set the Wheres
		$this->db->where('uid',$user_id);

		$query = $this->db->get('eventbooking_group');

		if($query->num_rows() > 0)
		{
			$array = array();
			foreach ( $query->result() as $row)
			{
				$array[] = $row->name;
			}
			return $array;
		}

		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}

}

/**
 *	GET SECTORS
 *  The function will return all the sectors that
 * 	are availble to the user at this time.
 *  @access Public
 *  @param 	string
 *				start 	=> start date and time
 *  @param 	string
 *				end 	=> end date and time
 *  @param 	string
 *				active  => if it is active or not.
 *  @param 	array	
 *				roles 	=> all the access roles the user has.			
 *  @return array
 *				return array of all the events.
 */
function get_sectors($eid = 1,$start,$end,$active = false,$roles = false)
{
	if (!isset($start))
	{
		$start  = date('Y-m-d H:i:s');
	}
	if (!isset($end))
	{
		$end  = date('Y-m-d H:i:s');
	}
	// Select Only Certain Fields
	$this->db->select('eventbooking_sectors.id as id,title,description,extrainformation,image,eventbooking_sectors.max as max,eid');

	// Join the tables needed.
	$this->db->join('eventbooking_bookingaccess','eventbooking_bookingaccess.sid=eventbooking_sectors.id');


	// Set the where's
	if ($active)
	{
		$this->db->where('active',$active);
	}
	if ($roles && is_array($roles))
	{
		$this->db->where_in('eventbooking_bookingaccess.id',$roles);
	}

	$this->db->where('eventbooking_sectors.start <=',$start);
	$this->db->where('eventbooking_sectors.end >=',$end);

	// Group By
	$this->db->group_by('id');

	// Sort By
	$this->db->order_by('order','ASC');
	$this->db->order_by('eventbooking_sectors.start','ASC');
	$this->db->order_by('id','ASC');

	$query = $this->db->get('eventbooking_sectors');

	if($query->num_rows() > 0)
	{
		return $query->result();
	}

	else
	{
		return false;
	}

}

/**
 *	GET SECTOR DETAILS
 *  The function will return the sector's details.
 *
 *  @access Public
 *  @param 	string
 *				sid 	=> sector id.			
 *  @return array
 *				return array of all the events.
 */
function get_sector_details($sid = 1)
{
	// Select Only Certain Fields
	$this->db->select('id,title,description,extrainformation,image,max,eid');

	$this->db->where('id',$sid);

	$query = $this->db->get('eventbooking_sectors');

	if($query->num_rows() > 0)
	{
		return $query->row_array();
	}

	else
	{
		return false;
	}

}

/**
 *	GET EVENT DETAILS
 *  The function will return the sector's details.
 *
 *  @access Public
 *  @param 	string
 *				sid 	=> sector id.			
 *  @return array
 *				return array of all the events.
 */
function get_event_details($sid = 1)
{
	// Select Only Certain Fields	
	$this->db->select('id,title,description,eventstart,eventend,image');

	$this->db->where('id',$sid);

	$query = $this->db->get('eventbooking_events');

	if($query->num_rows() > 0)
	{
		return $query->row_array();
	}

	else
	{
		return false;
	}

}
/**
 *	GET USER BOOKING
 *  The function will return the sector 
 *  that the user has already booked.
 *
 *  @access Public
 *  @param 	string
 *				uid 	=> the users id		
 *  @return 	string
 *				return string of the sector already booked or false.
 */
function get_user_booking($uid = false)
{
	//  Set the fields to be returned.
	$this->db->select('sid');
	
	// Set the filtered fields
	$this->db->where('uid',$uid);
	
	// Get the Results of the Quary
	$query = $this->db->get('eventbooking_bookings');
	
	if($query->num_rows() > 0)
	{
		$row = $query->row_array();
		return $row['sid'];
	}

	else
	{
		return false;
	}
	
	
}

/**
 *	GET NUMBER BOOKED IN EACH GROUP
 *  	The function will return the number from each 
 *	sector that have booked that sector.
 * 
 *  @access Public
 *  @param 	string
 *				sid 	=> the sector id			
 *  @return array
 *				return array of all the events.
 */
function get_number_booked_per_group($sid = false,$roles = array())
{
	// Set the Fields to be Returned. 
	$this->db->select('id,max,booked');

	// Set the filtered fields
	$this->db->where('sid',$sid);
	// Set the filtered fields
	$this->db->where_in('id',$roles);
	
	// Get the Results of the Query
	$query = $this->db->get('eventbooking_bookingaccess');
	
	if($query->num_rows() > 0)
	{
		return $query->result();
	}

	else
	{
		return false;
	}
	
}

/**
 *	GET NUMBER BOOKED IN SECTOR
 *  	The function will return the number from each 
 *	sector that have booked that sector.
 * 
 *  @access Public
 *  @param 	string
 *				sid 	=> the sector id			
 *  @return array
 *				return array of all the events.
 */
function get_sector_total($sid = false)
{
	// Set the fields to be returned.
	$this->db->select('id,max,count(id) as total');
	
	// Set the filter fields 
	$this->db->where('id',$sid);
	
	//Set the group by fields
	$this->db->group_by('id');

	// Get the Results of the Query
	$query = $this->db->get('eventbooking_sectors');

	if($query->num_rows() > 0)
	{
		$row = $query->row_array();
		return $row;
	}

	else
	{
		return false;
	}
}

/**
 *	GET NUMBER BOOKED IN EACH GROUP
 *  	The function will return the number from each 
 *	sector that have booked that sector.
 * 
 *  @access Public
 *  @param 	string
 *				sid 	=> the sector id			
 *  @return array
 *				return array of all the events.
 */
function get_total_number_booked($eid = false)
{
	// Set the fields to be returned.
	$this->db->select('id,max,count(id) as total');
	
	//Set the joins.
	$this->db->join('eventbooking_bookings','eventbooking_bookings.sid=eventbooking_bookings');
	
	// Set the filter fields 
	$this->db->where('eid',$eid);
	
	//Set the group by fields
	$this->db->group_by('eid');

	if($query->num_rows() > 0)
	{
		return $query->result();
	}

	else
	{
		return false;
	}
}

/**
 *	GET NUMBER BOOKED IN EACH GROUP
 *  	The function will return the number from each 
 *	sector that have booked that sector.
 * 
 *  @access Public
 *  @param 	string
 *				sid 	=> the sector id			
 *  @return array
 *				return array of all the events.
 */
function save_booking($sid = false,$uid = false,$booked = false,$roles = array())
{
	if ($booked)
	{
		//  Set the where.
		$this->db->where('sid',$booked);
		$this->db->where('uid',$uid);
		
		// Update the system.
		 $this->db->update('eventbooking_bookings',array('sid'=>$sid));
	}
	else
	{
		// Insert the booking into the system
		$this->db->insert('eventbooking_bookings',array('sid'=>$sid,'uid'=>$uid));
	}
	
	// Update the number chosen this sector in the groups.
	$this->db->where('sid',$sid);
	$this->db->where_in('id',$roles);
	
	// Update the system.
	$this->db->set('booked','`booked`+1',FALSE);
	$this->db->update('eventbooking_bookingaccess');

	if ($booked)
	{
		// Update the number chosen this sector in the groups.
		$this->db->where('sid',$booked);
		$this->db->where_in('id',$roles);
	
		// Update the system.
		$this->db->set('booked','`booked`-1',FALSE);
		$this->db->update('eventbooking_bookingaccess');
		
	}

	return true;
	
}

/** ************ THE END ************* **/

}
