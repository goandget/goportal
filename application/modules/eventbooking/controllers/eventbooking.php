<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Controller for the EVENT BOOKING APPLICATION
| -------------------------------------------------------------------
| This file will contain all the functions to connect with the 
| Database relevant to the event booking application.
*/

class eventbooking extends Controller {
	
	function __construct()
	{
		parent::Controller();

		// Load the Event Booking Configuration File
		$this->config->load('eventbooking',TRUE);

		// Check User is logged in.
		modules::run('login/is_logged_in');
		
		// Load Eventbooking model
		$this->load->model('eventbooking_model');

		// Get Current Users Details
		$this->details = modules::run('login/get_user_details');	

		$roles = $this->eventbooking_model->get_users_groups($this->details['id']);
		if (is_array($roles))
		{
			$this->details['roles'] = array_merge($this->details['roles'],$roles);
		}
		
	}
	
	function index()
	{
		// Load Eventbooking Model
		$events = $this->eventbooking_model->get_events(date("Y-m-d H:i:s"), date("Y-m-d H:i:s"),1,$this->details['roles']);
		// If there are more than one event applicable to the user
		// Display the list of events.
		if ($events && count($events) > 1)
		{
			$data['events'] = $events;
			$this->_display('events',$data);
		}
		// If there is only one event then display the sectors for that event.
		else if ($events && count($events) == 1)
		{
			$this->viewSectors($events[0]->id);
		} 
		else
		{
			$this->_display('error',array('title'=>'Event Error C48','messages'=>'There are no events for you at this moment. <br />If you think this is a mistake then please contact one of the IT Technicians.'));
		}
	}
	
	function viewSectors($eid = false)
	{
		//echo $eid.'hi';
		//die;
		// If no event id is provided then return an error
		if ($eid)
		{
			// Clear the data array.
			$data = array();
			// Get the event details.
			$data['event'] = $this->eventbooking_model->get_event_details($eid);

			//  Get the sector that the users is booked on.
			$data['userbooked'] = $this->eventbooking_model->get_user_booking($this->details['id']);
			
			// Get the sectors that the user is able to book for this event
			$sectors = $this->eventbooking_model->get_sectors($eid,date("Y-m-d H:i:s"), date("Y-m-d H:i:s"),1,$this->details['roles']);
			foreach ($sectors as $sector)
			{
				$total = $this->eventbooking_model->get_sector_total($sector->id);
				
				$RoleMaxes = $this->eventbooking_model->get_number_booked_per_group($sector->id,$this->details['roles']);
				unset($dif);
				foreach ($RoleMaxes as $role)
				{
					if (isset($dif)&&($role->max - $role->booked < $dif))
					{
						$dif = $role->max - $role->booked;
						$rolemax = $role->max;
						$rolebooked = $role->booked;
					}
					else if (!isset($dif))
					{
						$dif = $role->max - $role->booked;
						$rolemax = $role->max;
						$rolebooked = $role->booked;
					}
				}
				// Set the Data to the row
				$sector->total = $total;
				$sector->rolemax = $rolemax;
				$sector->rolebooked = $rolebooked;
				$colors = $this->config->item('colors','eventbooking');
				$sector->color = $colors[array_rand($colors)];

				// Now load the data into the data array
				$data['sectors'][] = $sector;
				
			}

			$this->_display('sectors',$data);

		}
		else
		{
			$this->_display('error',array('title'=>'Sector Error C105','messages'=>'There are no sectors for you at the moment. <br />If you think this is a mistake then please contact one of the IT Technicians.'));
		}
	}
	
	function book()
	{
		if (!($book = $this->eventbooking_model->get_user_booking($this->details['id'])))
		{
			$book = FALSE;
		}

		if ($this->_fullcheck($this->uri->segment(3)))
		{
			$this->_ajaxDisplay('error',array('title'=>'The Sector is now full','messages'=> 'The sector is now fully booked. If you think this is an error please refresh the page and try again.','book'=>$book));
		}
		else
		{
			// Save the Booking
			$result = $this->eventbooking_model->save_booking($this->uri->segment(3),$this->details['id'],$book,$this->details['roles']);
			$sector = $this->eventbooking_model->get_sector_details($this->uri->segment(3));

			if (($result)&&($sector))
			{
				$this->_ajaxDisplay('booking',array('title'=>'Your Booking has been successful','messages'=>'You have been successfully booked on to the '.$sector['title'].' sector','book'=>$book));
			}
			else if ($sector)
			{
				$this->_ajaxDisplay('error',array('title'=>'Booking Error C138: Your booking was unsuccessful','messages'=>'There was an error booking your '.$sector['title'].' sector, if the problem persists then please contact an IT Technician and tell them that the Error was: eventbooking00'.$this->input->get_post('sid',TRUE),'book'=>$book));
			}
			else
			{
				$this->_ajaxDisplay('error',array('title'=>'Booking Error C142','messages'=> 'There was no sector chosen, if the problem persists then please contact an IT Technician and tell them that the Error was: eventbooking00'.$this->input->get_post('sid',TRUE),'book'=>$book));
			}
		}
	}

	private function _fullcheck($sid)
	{
		$total = $this->eventbooking_model->get_sector_total($sid);
		
		if ($total['max'] == $total['total']) {
			return true;
		}
		
		$RoleMaxes = $this->eventbooking_model->get_number_booked_per_group($sid,$this->details['roles']);

		foreach ($RoleMaxes as $role)
		{
			if ($role->max - $role->booked == 0)
			{
				return true;
			}
		}

		return false;

	}

	private function _display($template = 'error',$data)
	{
		if ($template == 'error')
		{
			$this->template->add_js('Default/js/error');
			$this->template->add_css('Default/css/error');
		}
		$this->template->set_content($template, $data);
		$this->template->add_js('eventbooking/js/eventbooking');
		$this->template->add_css('eventbooking/css/eventbooking');
		$this->template->add_css('eventbooking/css/ie',true,true);
		$this->template->build();
	}

	private function _ajaxDisplay($type = 'error',$data)
	{
		$this->output->set_status_header(200);
    	$this->output->set_header('Content-type: application/json');
	    $this->output->set_output(json_encode(array('type' => $type,'title' => $data['title'],'message'=> $data['messages'],'book'=> $data['book'])));
	}

/** ************ THE END ************* **/

}	
