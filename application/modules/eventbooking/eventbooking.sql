-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 01, 2013 at 12:29 PM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `goPortal`
--

-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_bookingaccess`
--

CREATE TABLE IF NOT EXISTS `eventbooking_bookingaccess` (
  `sid` int(6) NOT NULL,
  `id` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `max` int(6) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `booked` int(6) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`sid`,`id`,`type`,`start`,`end`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventbooking_bookingaccess`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_bookings`
--

CREATE TABLE IF NOT EXISTS `eventbooking_bookings` (
  `sid` int(6) NOT NULL,
  `uid` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventbooking_bookings`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_group`
--

CREATE TABLE IF NOT EXISTS `eventbooking_group` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eventbooking_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_groupuser`
--

CREATE TABLE IF NOT EXISTS `eventbooking_groupuser` (
  `gid` int(6) NOT NULL,
  `uid` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`gid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventbooking_groupuser`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_sectors`
--

CREATE TABLE IF NOT EXISTS `eventbooking_sectors` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `eid` int(6) NOT NULL,
  `title` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(245) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extrainformation` text COLLATE utf8_unicode_ci,
  `image` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `max` int(6) NOT NULL DEFAULT '1',
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `order` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eventbooking_sectors`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooking_tracking`
--

CREATE TABLE IF NOT EXISTS `eventbooking_tracking` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` int(6) NOT NULL,
  `uid` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eventbooking_tracking`
--


-- --------------------------------------------------------

--
-- Table structure for table `eventbooling_events`
--

CREATE TABLE IF NOT EXISTS `eventbooking_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `eventstart` datetime NOT NULL,
  `eventend` datetime NOT NULL,
  `image` varchar(145) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eventbooling_events`
--

-- --------------------------------------------------------

INSERT INTO `eventbooking_bookingaccess` (`sid`, `id`, `type`, `max`, `start`, `end`, `booked`, `active`) VALUES
(2, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-14 12:28:54', 1, 1),
(1, 'red', 'booking', 2, '2013-03-07 12:28:49', '2013-03-14 12:28:54', 2, 1);

-- --------------------------------------------------------


INSERT INTO `eventbooking_bookings` (`sid`, `uid`, `updated`) VALUES
(1, '1', '2013-03-11 07:13:48');

-- --------------------------------------------------------

INSERT INTO `eventbooking_events` (`id`, `title`, `description`, `eventstart`, `eventend`, `image`, `start`, `end`, `active`) VALUES
(1, 'Flexible Day', 'The Staff Selection for Flexible Day.', '2013-07-10 12:25:48', '2013-07-10 12:25:48', 'flexidays', '2013-03-07 12:26:41', '2013-03-16 12:26:45', 1);

-- --------------------------------------------------------

INSERT INTO `eventbooking_group` (`id`, `name`) VALUES
(1, 'red');

-- --------------------------------------------------------

INSERT INTO `eventbooking_groupuser` (`gid`, `uid`) VALUES
(1, '1');

-- --------------------------------------------------------


INSERT INTO `eventbooking_sectors` (`id`, `eid`, `title`, `description`, `extrainformation`, `image`, `max`, `start`, `end`, `order`) VALUES
(1, 1, 'NHS', 'Erm', NULL, '1-NHS.jpg', 12, '2013-03-07 12:27:45', '2013-03-14 12:27:49', 1),
(2, 1, 'A Nother', 'A N Other', NULL, '1-NHS.jpg', 1, '2013-03-10 18:24:18', '2013-03-28 18:24:21', 1);
