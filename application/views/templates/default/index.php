<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" class="no-js">
<head>
	<meta charset="utf-8">
	<title><?php echo $site_title?> - <?php echo $site_name?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <base href="<?php echo base_url();?>" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Default/css/normalize.css" type="text/css" media="screen,projection" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Default/css/main.css" type="text/css" media="screen,projection" />
	<?php echo $head?>
	<?php echo $css?>
</head>

<body>
 <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
 <![endif]-->
    <div id="alerts">
        <div class="title"></div>
        <div class="message"></div>
    </div>
    <div id="mainContent">
        <!-- Header Content Area -->
          <div id="header-container">
            <div id="header-thin-border"></div>
            <div id="header-border">
                <div id="header" class="container-area">
                    <div id="header-menu" class="float-r">
                        <ul class="menu-inline">
                            <li><?php if ($this->session->userdata('is_logged_in')) {?><a href="<?php echo site_url('login/logout') ?>">Log out</a><?php } ?></li>
                        </ul>
                    </div>
                    </div>
                        <img src="<?php echo base_url();?>assets/Default/img/logo.jpg" alt="St Edmund Arrowsmith Catholic High School" class="float-l"/>
                        <?php if ($this->session->userdata('is_logged_in')) {?><a class="float-r" href="#" id="toggleapps">My apps</a><?php } ?>
                    <div class="clr-b"></div>
                </div>
            </div>
          </div>

<?php 
	if (isset($messages))	{
		echo $messages;
	}
?>
            <?php if ($this->session->userdata('is_logged_in')) {?>
            <div id="applayer">
                <div class="grid_12">
                    <!--<h2>My Apps</h2>-->
                    <ul class="apps">
                        <li><a href="" class="applink"><img src="<?php echo base_url();?>assets/Default/img/icons/home.png" alt="Home" /><br />Home</a></li>
                        <li><a href="<?php echo site_url('senregister/') ?>" class="applink"><img src="<?php echo base_url();?>assets/Default/img/icons/SENRegister.png" alt="SEN Register" /><br />SEN Register</a></li>
                        <li><a href="<?php echo site_url('eventbooking/') ?>" class="applink"><img src="<?php echo base_url();?>assets/Default/img/icons/eventBooking.png" alt="Event Booking System" /><br />Event Booking</a></li>
                        <li><a href="http://legacywebserver/" class="applink"><img src="<?php echo base_url();?>assets/Default/img/icons/OldIntranet.png" alt="Old Intranet" /><br />Old Intranet</a></li>
                    </ul>
                
                </div>
            </div>
            <?php } ?>
        <div id="contentlayer">
		    <?php echo $content?>
            <div class="clr-b ftsize">&nbsp;</div>
        </div>
        <!-- Footer Content Area -->
        <div id="footer-container">
            <div id="footer-content-border">
                <div id="footer-content" class="container-area">
                     <div class="float-l">
                    <img src="<?php echo base_url();?>assets/Default/img/ftlogo.png" alt="Eddies School Logo" /></div>
                    <!-- Footer Menu -->
                    <jdoc:include type="modules" name="menu-bottom" />
                    <div class="float-r ft-txt"><p>Page rendered in {elapsed_time} seconds</p>
                    <jdoc:include type="modules" name="position-7" />
                    <div class="clr-b"></div>
                </div>
            </div>
        </div>
    </div>
        <!-- Include All Required Javascript Files -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/Default/js/vendor/jquery-1.8.0.min.js"><\/script>')</script>
        <script src="<?php echo base_url();?>assets/Default/js/plugins.js"></script>
        <script src="<?php echo base_url();?>assets/Default/js/main.js"></script>
        <script src="<?php echo base_url();?>assets/Default/js/vendor/modernizr-2.6.1.min.js"></script>
        <?php echo $js?>
</body>
</html>
