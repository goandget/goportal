$(document).ready(function() {
	var appLayerActive = 0;
	var appLayerHeight = $("#applayer").height();
	var headerHeight   = $("#header-container").height();
	var scrollHeight   = appLayerHeight+headerHeight

	$('#alerts').hide();
	
	$("#toggleapps").click(function(e){  
		e.preventDefault();
			
		if(appLayerActive == 0) {
			$("#contentlayer").animate({
				top:'+'+scrollHeight	
			},1000, 'easeOutBounce', function() {
				appLayerActive = 1;	
			});
					}
		else {
			$("#contentlayer").animate({
				top: +headerHeight	
			},1000, 'easeOutBack', function() {
				appLayerActive = 0;	
			});
			
		}
		
	});
});
