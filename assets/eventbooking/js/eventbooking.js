$(".bookable").click(bookEvent); 

function bookEvent() 
{
	// Stop Animation on Alerts.
	$('#alerts').stop(true,true);
	// URL of the Ajax
    var jsonUrl = "/eventbooking/book/"+$(this).attr('id'); 
    var sid = $(this).attr('id');
    // Get Ajax of Result
	$.getJSON(  
	    jsonUrl,    
	    function(json) {  
	        if (json.type == 'error')
	        {
	        	$('#alerts').addClass('errors');
	        }

	        $('#'+sid).removeClass('bookable');
	        $('#'+sid+' .ribbon').addClass('booked-ribbon');
	        $('#'+sid).unbind('click');
	        $('#'+json.book).addClass('bookable');
	        $('#'+json.book+' .ribbon').removeClass('booked-ribbon');
	        $('#'+json.book).click(bookEvent);
	        $('#'+json.book +' .ribbon').html('BOOK NOW');
	        $('#'+sid +' .ribbon').html('BOOKED');
	        $('#alerts .title').html('Result: '+json.title);
	        $('#alerts .message').html(json.message);
	        $('#alerts').slideDown('slow').delay('5000').slideUp('slow');
	    }  
	);  
	return false;  
}