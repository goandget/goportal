$(document).ready(function() 
 {
    $('#displaycontent ul li.menucls').click(function(e) 
    { 
     get_classdetail($(this).text());
    });
    $('#displaycontent ul li.menucls').addClass('link');

    $('.pupildetail').click(function(e) 
    { 
    	$(this).parent().children('.pupilneed').addClass('hidden');
    });
     $('ul.needs li').click(function(e)
    {
     $('.pupilneed').addClass('hidden').html('');
     get_needdetail($(this).text().replace(",",""),this);
    });
     $('ul.needs li').addClass('link');

 });


function get_classdetail(cls)
{	
	$.ajax({
	  type: "POST",
	  url: "/senregister/ajax_classdetail",
	  data: { curclass: cls }
	}).done(function( data ) {
	  $('#displaycontent ul').find('li.active').removeClass('active');
	  $('#displaycontent ul li:contains("'+cls+'")').filter(function() {
  		return $(this).text() == cls;
	  }).addClass('active');
	  $('div.pupildetail-container').html(data);
	    $('ul.needs li').click(function(e)
	    {
	  	 $('.pupilneed').addClass('hidden').html('');
	     get_needdetail($(this).text().replace(",",""),this)
	    });
     	$('ul.needs li').addClass('link');

	    $('.pupildetail').click(function(e) 
	    { 
	    	$(this).parent().children('.pupilneed').addClass('hidden');
	    });
	});

}

function get_needdetail(need,id)
{
	needarea = $(id).parent().parent().parent().parent().children('div.pupilneed');

	$.ajax({
	  type: "POST",
	  url: "/senregister/ajax_needdetails",
	  data: { needs: need }
	}).done(function( data ) {
		needarea.removeClass('hidden').html(data);

		/* Set up Click Functionality */
		$('.needwhatmenu').click(function(e)
	    {
	     view_needdetails('needwhat');
	    });
	    $('.needwhatmenu').addClass('needlink');
		$('.needhowmenu').click(function(e)
	    {
	     view_needdetails('needhow');
	    });
	    $('.needhowmenu').addClass('needlink');
		$('.needfurthermenu').click(function(e)
	    {
	     view_needdetails('needfurther');
	    });
	    $('.needfurthermenu').addClass('needlink');
	});
}

function view_needdetails(detail)
{
	$('.needwhat').removeClass('hidden').addClass('hidden');
	$('.needhow').removeClass('hidden').addClass('hidden');
	$('.needfurther').removeClass('hidden').addClass('hidden');
	$('.'+detail).removeClass('hidden');
	$('.needdetail').removeClass('needwhatcolor').removeClass('needhowcolor').removeClass('needfurthercolor').addClass(detail+'color');
}